@extends('admin.layout')
@section('pagename') View all Apartments @stop
@section('content')
<hr>
           <div class="table-responsive">
            <table class="table table-striped display" id="datatables">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Apartment name</th>
                  <th>Location</th>
                  <th>Name of Landlord</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @php 
            $i=0;
              @endphp
              @foreach($apartments as $apartments)
              @php
              $i +=1;
              @endphp
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$apartments->name}}</td>
                  <td>{{$apartments->location}}</td>
                  <td>{{$apartments->fname}}</td>
                  <td style="width:180px;">
              <a href="{{url('/editApartment/'.$apartments->id)}}" class="btn btn-success"><i class="fa fa-fw fa-edit"></i>Edit</a></td>
                              
                </tr>
               @endforeach
              </tbody>
            </table>
          </div>
          @stop