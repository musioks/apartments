@extends('admin.layout')
@section('pagename') Add Landlord @stop
@section('content')
          <form  enctype="multipart/form-data" method="post" action="{{url('/addlandlord')}}">
                {{csrf_field()}}
                <div class="box-body">
                 <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="form-group">
                        @if(Session::has('error'))
                            <div class="alert alert-danger text-center">
                              <i class="fa fa-fw fa-window-close"></i>  {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success text-center">
                            <i class="fa fa-fw fa-check"></i> 
                                {{Session::get('success')}}
                            </div>
                        @endif
                    </div>
                </div>
                </div><!--end row-->
                <div class="row">
                       <div class="col-md-6">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('name') ? 'has-error':''}}">
                        <label>Full Name *</label>
                        <input type="text" name="name" class="form-control" placeholder="eg. John Doe">
                        @if($errors->has('name'))
              <span class="help-block">{{$errors->first('name')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <div class="col-sm-6">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('phone') ? 'has-error':''}}">
                        <label> Phone No.*</label>
                        <input type="text" name="phone" class="form-control" placeholder="Phone No.">
                        @if($errors->has('phone'))
              <span class="help-block">{{$errors->first('phone')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                
                 
                </div><!--end row--> 

                <div class="row">
             
                       <div class="col-md-4">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('salary') ? 'has-error':''}}">
                        <label>Salary Amount *</label>
                        <input type="text" name="salary" class="form-control" placeholder="salary">
                        @if($errors->has('salary'))
              <span class="help-block">{{$errors->first('salary')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                     <div class="col-sm-4">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('email') ? 'has-error':''}}">
                        <label> Email*</label>
                        <input type="email" name="email" class="form-control" placeholder="Email Address">
                        @if($errors->has('email'))
              <span class="help-block">{{$errors->first('email')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                 <div class="col-md-4">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('password') ? 'has-error':''}}">
                        <label>Password*</label>
                        <input type="password" name="password" class="form-control" placeholder="e.g landlord123">
                        @if($errors->has('password'))
              <span class="help-block">{{$errors->first('password')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                </div><!--end row-->  

                <div class="row">                
                <div class="col-md-6 col-md-offset-5">

                    <div class="form-group">

                        <button type="submit" class="btn btn-success" >Add Landload</button>
                    </div>
                </div>

                <!-- /.form-group -->
                     </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
            </form>
          @stop