<?php

namespace App\Http\Controllers;

use App\Tenant;
use App\House;
use Sentinel;
use DB;
use PDF;
use Illuminate\Http\Request;

class TenantController extends Controller
{
    public function __construct(){
        $this->middleware('admin');
    }
    public function tenants()
    {
          $tenants=DB::table('tenants')
          ->join('houses','tenants.house_id','=','houses.id')
          ->join('apartments','houses.apartment_id','=','apartments.id')
          ->select('tenants.*','houses.houseID','apartments.name')
          ->get();
      return view('admin.tenants',['tenants'=>$tenants]);
        
        
    }
    public function addtenant()
    {
       $houses=DB::table('houses')->join('apartments','houses.apartment_id','=','apartments.id')
    ->select('houses.*','apartments.name as fname','apartments.location')
    ->where('status','=','Vacant')
    ->get();
        return view('admin.addtenant',['houses'=>$houses]);
        
    }
    public function storeTenant(Request $request)
    {
          $this->validate($request,[
        'fname'=>'required',
        'house_id'=>'required|unique:tenants',
        'phone'=>'required|unique:tenants',
        'email'=>'required|unique:tenants',
        'address'=>'required',
        'nok'=>'required',
        'password'=>'required',
        ]);

             DB::transaction(function() use($request){
            $credentials=[
            'name'=>$request->fname,
            'phone'=>$request->phone,
            'email'=>$request->email,
            'password'=>$request->password,

            ];
       $user=Sentinel::registerAndActivate($credentials);
        $role=Sentinel::findRoleBySlug('tenant');
        $role->users()->attach($user);
                $tenant=new Tenant();
       
           $tenant->user_id=$user->id;
           $tenant->house_id=$request->house_id;
           $tenant->fname=$request->fname;
           $tenant->phone=$request->phone;
           $tenant->email=$request->email;
           $tenant->address=$request->address;
           $tenant->nok=$request->nok;
           $tenant->save();
           
     });
        return redirect()->back()->with('success','Tenant Added Successfully!');
    }
      public function tenantsReport(){
            $tenants=DB::table('tenants')
          ->join('houses','tenants.house_id','=','houses.id')
          ->join('apartments','houses.apartment_id','=','apartments.id')
          ->select('tenants.*','houses.houseID','apartments.name')
          ->get();
            $pdf=PDF::loadView('admin.tenantsReport',['tenants'=>$tenants])->setPaper('a4', 'landscape');
            return $pdf->stream('tenants.pdf');
   }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
    }
}
