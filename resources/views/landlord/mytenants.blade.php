@extends('landlord.layout')
@section('pagename') My Tenants @stop
@section('content')
           <div class="table-responsive">
            <table class="table table-striped display" id="datatables">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Full Name</th>
                  <th>Phone No.</th>
                  <th>Email Address</th>
                  <th>Postal Address</th>
                  <th>Next of Kin</th>
                  <th>House No.</th>
                  <th>Apartment Name</th>
                </tr>
              </thead>
              <tbody>
              @php 
            $i=0;
              @endphp
              @foreach($tenants as $tenants)
              @php
              $i +=1;
              @endphp
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$tenants->fname}}</td>
                  <td>{{$tenants->phone}}</td>
                  <td>{{$tenants->email}}</td>
                  <td>{{$tenants->address}}</td>
                  <td>{{$tenants->nok}}</td>
                  <td>{{$tenants->houseID}}</td>
                  <td>{{$tenants->name}}</td>        
              
                </tr>
               @endforeach
              </tbody>
            </table>
          </div>
          @stop