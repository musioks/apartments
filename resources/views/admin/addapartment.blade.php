@extends('admin.layout')
@section('pagename') Add Apartment @stop
@section('content')
          <form  enctype="multipart/form-data" method="post" action="{{url('/addapartment')}}">
                {{csrf_field()}}
                <div class="box-body">
                 <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        @if(Session::has('error'))
                            <div class="alert alert-danger text-center">
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success text-center">
                                {{Session::get('success')}}
                            </div>
                        @endif
                    </div>
                </div>
                </div><!--end row-->
                <div class="row">
                       <div class="col-md-6">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('name') ? 'has-error':''}}">
                        <label>Apartment Name *</label>
                        <input type="text" name="name" class="form-control" placeholder="eg. Belle Apartments">
                        @if($errors->has('name'))
              <span class="help-block">{{$errors->first('name')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <div class="col-sm-6">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('location') ? 'has-error':''}}">
                        <label> Where is it located? *</label>
                        <input type="text" name="location" class="form-control" placeholder="eg. Along Gong RD near Prestige">
                        @if($errors->has('location'))
              <span class="help-block">{{$errors->first('location')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                
                 
                </div><!--end row--> 

                <div class="row">
                  <div class="col-sm-12">
                 <!-- /.form-group -->
                    <div class="form-group">
                        <label> Landlord in charge*</label>
                        <select name="landlord_id" class="form-control">
                         @foreach($lords as $lords)
                         <option value="{{$lords->id}}">{{$lords->name}}</option>
                         @endforeach
                        </select>
                       
                    </div>
                <!-- /.form-group -->
                </div>
                     
                </div><!--end row-->  

                <div class="row">                
                <div class="col-md-6 col-md-offset-5">

                    <div class="form-group">

                        <button type="submit" class="btn btn-success" >Add Apartment</button>
                    </div>
                </div>

                <!-- /.form-group -->
                     </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
            </form>
          @stop