<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Sentinel;
class AController extends Controller
{
   public function __construct(){
      $this->middleware('lord');
    }
    public function index()
    {
   $user=Sentinel::getUser();
    $apartments=DB::table('apartments')->join('landlords','apartments.landlord_id','=','landlords.id')
    ->select('apartments.*','landlords.name as fname','landlords.email','landlords.phone')
    ->where('email','=',$user->email)
    ->get();
     $houses=DB::table('houses')->join('apartments','houses.apartment_id','=','apartments.id')
     ->join('landlords','apartments.landlord_id','=','landlords.id')
    ->select('houses.*','apartments.name','apartments.location','landlords.email')
    ->where('landlords.email','=',$user->email)
    ->get();
       $tenants=DB::table('tenants')
          ->join('houses','tenants.house_id','=','houses.id')
          ->join('apartments','houses.apartment_id','=','apartments.id')
          ->join('landlords','apartments.landlord_id','=','landlords.id')
          ->select('tenants.*','houses.houseID','apartments.name','landlords.email as mail','landlords.user_id')
          ->where('landlords.email','=',$user->email)
          ->get();

        return view('landlord.index',['houses'=>$houses,'tenants'=>$tenants,'apartments'=>$apartments]);
    }
    public function fetchpayments()
    {
        return view('landlord.fetchpayments');
        //
    }
      public function mytenants()
    {
         $user=Sentinel::getUser();
          $tenants=DB::table('tenants')
          ->join('houses','tenants.house_id','=','houses.id')
          ->join('apartments','houses.apartment_id','=','apartments.id')
          ->join('landlords','apartments.landlord_id','=','landlords.id')
          ->select('tenants.*','houses.houseID','apartments.name','landlords.email as mail','landlords.user_id')
          ->where('landlords.email','=',$user->email)
          ->get();
        return view('landlord.mytenants',['tenants'=>$tenants]);
        //
    }
      public function myapartments()
    {
           $user=Sentinel::getUser();
    $apartments=DB::table('apartments')->join('landlords','apartments.landlord_id','=','landlords.id')
    ->select('apartments.*','landlords.name as fname','landlords.email','landlords.phone')
    ->where('email','=',$user->email)
    ->get();
        return view('landlord.myapartments',['apartments'=>$apartments]);
        //
    }
      public function myrooms()
    {
         $user=Sentinel::getUser();
    $houses=DB::table('houses')->join('apartments','houses.apartment_id','=','apartments.id')
     ->join('landlords','apartments.landlord_id','=','landlords.id')
    ->select('houses.*','apartments.name','apartments.location','landlords.email')
    ->where('landlords.email','=',$user->email)
    ->get();
        return view('landlord.myrooms',['houses'=>$houses]);
        //
    }
     public function notifications()
    {
        $notices=\App\Announcement::all();
        return view('landlord.notifications',['notices'=>$notices]);
        //
    }
      public function maintenance()
    {
        $user=Sentinel::getUser();
        $data=DB::table('maintenances')->join('tenants','maintenances.tenant_id','=','tenants.id')
     ->join('houses','tenants.house_id','=','houses.id')
     ->join('apartments','houses.apartment_id','=','apartments.id')
     ->join('landlords','apartments.landlord_id','=','landlords.id')
    ->select('maintenances.*','tenants.fname','tenants.phone','tenants.email','houses.houseID',
        'apartments.name','apartments.location','landlords.email')
    ->where('landlords.email','=',$user->email)
    ->get();
        return view('landlord.maintenance',['data'=>$data]);
        //
    }
        public function complains()
    {
        $user=Sentinel::getUser();
        $data=DB::table('complains')->join('tenants','complains.tenant_id','=','tenants.id')
     ->join('houses','tenants.house_id','=','houses.id')
     ->join('apartments','houses.apartment_id','=','apartments.id')
     ->join('landlords','apartments.landlord_id','=','landlords.id')
    ->select('complains.*','tenants.fname','tenants.phone','tenants.email','houses.houseID',
        'apartments.name','apartments.location','landlords.email')
    ->where('landlords.email','=',$user->email)
    ->get();
        return view('landlord.complains',['data'=>$data]);
        //
    }
    
}
