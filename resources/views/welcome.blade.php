
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

     <title>ONLINE APARTMENT MANAGEMENT SYSTEM</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('theme/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font awesome CSS -->
    <link href="{{asset('theme/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{asset('theme/css/carousel.css')}}" rel="stylesheet">
  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <nav class="navbar navbar-default navbar-static-top" style="border-radius:0px;">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{url('/')}}">ONLINE APARTMENT MANAGEMENT SYSTEM</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse ">
              <ul class="nav navbar-nav">
                <li class="active"><a href="{{url('/')}}">Home</a></li>

              </ul>
               <ul class="nav navbar-nav navbar-right">
               <li ><a href="{{url('/pages/apartments')}}">Apartments</a></li>
               <li ><a href="{{url('/pages/rooms')}}">Vacant Rooms</a></li>
                
                <li><a href="{{url('/login')}}"><i class="fa fa-fw fa-rocket"></i>Login</a></li>
                 <li><a href=""></a></li>
              

              </ul>
            </div>
          </div>
        </nav>

      </div>
    </div>


    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="{{asset('images/veridian.jpg')}}" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>The veridian Apartments</h1>
             
              <p><a class="btn btn-lg btn-success" href="{{url('/pages/apartments')}}" role="button">View details</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="second-slide" src="{{asset('images/cityview.jpg')}}" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>City View Apartments</h1>
             
              <p><a class="btn btn-lg btn-success" href="{{url('/pages/apartments')}}" role="button">View details</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="third-slide" src="{{asset('images/hesby.jpg')}}" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>The Hesby Apartments</h1>
             
              <p><a class="btn btn-lg btn-success" href="{{url('/pages/apartments')}}" role="button">View details</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->


    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
          <img class="img-circle" src="{{asset('images/promenade.jpg')}}" alt="Generic placeholder image" width="140" height="140">
          <h2>Promenade Apartments</h2>
          <p>The development is nestled in the high end areas of thindigua, 300 meters off kiambu road.  these are four block apartments with a total of 54 houses.  comprises of 6 penthouses. The houses are 2 bedroom apartments, with 2 bathrooms (one bedroom en-suite and a visitors cloackroom)ample parking space is available. </p>
          <p><a class="btn btn-warning" href="{{url('/pages/rooms')}}" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="{{asset('images/belle.jpg')}}" alt="Generic placeholder image" width="140" height="140">
          <h2>The Belle Apartments</h2>
          <p>The apartment is located at corner of kikambala and mwingi roads in kileleshwa.  the apartment is a 3 bedroom duplex with a sitting lounge area kitchen, guest toilets and pantry on the lower floor. The upper floor has all the rooms. Both floors have their own balconies. The total floor size is around 200 square meters with balconies.  </p>
          <p><a class="btn btn-warning" href="{{url('/pages/rooms')}}" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="{{asset('images/moda.jpg')}}" alt="Generic placeholder image" width="140" height="140">
          <h2>Moda Apartments</h2>
          <p>Brand new apartment, completely renovated in 2012 and tastefully furnished, with attention to details, modern colours, designer lighting and high quality accessories. Located in the centre of Kisumu, perfect for those looking for a functional and high standard accommodation. </p>
          <p><a class="btn btn-warning" href="{{url('/pages/rooms')}}" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->


      
<hr>
      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2017 APARTMENT MANAGEMENT SYSTEM. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  <script src="{{asset('theme/js/jquery.min.js')}}"></script>
    <script src="{{asset('theme/js/bootstrap.min.js')}}"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="{{asset('theme/js/holder.min.js')}}"></script>
  </body>
</html>
