<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
class PagesController extends Controller
{
    
    public function index(){
       
    	return view('welcome');
    }
      public function houses(){
         $houses=DB::table('houses')->join('apartments','houses.apartment_id','=','apartments.id')
    ->select('houses.*','apartments.name as fname','apartments.location')
    ->where('status','=','Vacant')
    ->get();
    	return view('houses',['houses'=>$houses]);
    }
      public function apartments(){
      	 $apartments=DB::table('apartments')->join('landlords','apartments.landlord_id','=','landlords.id')
    ->select('apartments.*','landlords.name as fname','landlords.phone')
    ->get();
       
    	return view('apartments',['apartments'=>$apartments]);
    }
   
}
