<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Landlords Report</title>
@include('admin.bts')
</head>
<body>
<div class="container-fluid">
<h3 class="text-center text-success">ONLINE APARTMENT MANAGEMENT SYSTEM </h3>
<hr>
<h4 class="text-center">LANDLORDS LIST</h4>
<hr>
<table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Full Name</th>
                  <th>Phone</th>
                  <th>Email Address</th>
                  <th>Salary</th>
                </tr>
              </thead>
              <tbody>
              @php 
            $i=0;
              @endphp
              @foreach($lords as $lords)
              @php
              $i +=1;
              @endphp
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$lords->name}}</td>
                  <td>{{$lords->phone}}</td>
                  <td>{{$lords->email}}</td>
                  <td>Kshs.{{$lords->salary}}</td>
              
                </tr>
               @endforeach
              </tbody>
            </table>
                    </div>
</body>
</html>