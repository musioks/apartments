<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('pageNotFound', 'ErrorController@pageNotFound')->name('notFound');
Route::get('/', 'PagesController@index');
Route::get('/pages/rooms', 'PagesController@houses');
Route::get('/pages/apartments', 'PagesController@apartments');

Route::get('/signup', 'RegisterController@create')->name('signup');
Route::post('/signup', 'RegisterController@storeAdmin')->name('storeAdmin');
Route::get('/login', 'LoginController@login')->name('signin');
Route::post('/login', 'LoginController@Sign_In');
Route::post('/signout', 'LoginController@getLogout')->name('signout');

Route::get('/admin','AdminController@index')->name('admin.index');
///////////////////////----------------------Apartments------------------------------//////////////
Route::get('/addapartment','ApartmentController@addapartment')->name('addapartment');
Route::post('/addapartment','ApartmentController@storeApartment');
Route::get('/apartments','ApartmentController@apartments')->name('apartments');
Route::get('/editApartment/{id}','ApartmentController@editApartment')->name('editApartment');
Route::post('/editApartment/{id}','ApartmentController@updateApartment')->name('updateApartment');
/////////////////------------Landlord---------------------------///////////////////////////////
Route::get('/addlandlord','LandlordController@addlandlord')->name('addlandlord');
Route::post('/addlandlord','LandlordController@storeLandlord');
Route::get('/landlords','LandlordController@landlords')->name('landlords');
/////////////////------------Houses---------------------------///////////////////////////////
Route::get('/addhouse','HouseController@addhouse')->name('addhouse');
Route::post('/addhouse','HouseController@storeHouse');
Route::get('/houses','HouseController@houses')->name('houses');
/////////////////------------Tenants---------------------------///////////////////////////////
Route::get('/addtenant','TenantController@addtenant')->name('addtenant');
Route::post('/addtenant','TenantController@storeTenant');
Route::get('/tenants','TenantController@tenants')->name('tenants');
/////////////////------------Announcements---------------------------///////////////////////////////
Route::get('/createannouncement','AnnouncementController@createannouncement')->name('createannouncement');
Route::post('/createannouncement','AnnouncementController@storeAnnouncement');
Route::get('/announcements','AnnouncementController@announcements')->name('announcements');

/////////////////------------reports---------------------------///////////////////////////////
Route::get('/reports','AdminController@reports')->name('reports');
Route::get('/payments','AdminController@payments')->name('payments');

//////////////////////// PDFs------------------------------------//////////////////////////////
Route::get('/apartmentsReport','ApartmentController@apartmentsReport');
Route::get('/landlordsReport','LandlordController@landlordsReport');
Route::get('/tenantsReport','TenantController@tenantsReport');
Route::get('/housesReport','HouseController@housesReport');


/** -------------------------Landlords routes ----------------------------------*/

Route::get('/landlord','AController@index')->name('landlord.index');
Route::get('/mytenants','AController@mytenants')->name('mytenants');
Route::get('/myapartments','AController@myapartments')->name('myapartments');
Route::get('/myrooms','AController@myrooms')->name('myrooms');
Route::get('/notifications','AController@notifications')->name('notifications');
Route::get('/maintenance','AController@maintenance')->name('maintenance');
Route::get('/complains','AController@complains')->name('complains');


/** -------------------------Tenants routes ----------------------------------*/

Route::get('/tenants/index','TController@index')->name('tenant.index');
Route::get('/tenants/notices','TController@notices')->name('notices');
Route::get('/createRequest','TController@createRequest')->name('createRequest');
Route::post('/createRequest','TController@storeRequest')->name('storeRequest');
Route::get('/getRequests','TController@getRequests')->name('getRequests');

Route::get('/postComplain','TController@postComplain')->name('postComplain');
Route::post('/postComplain','TController@storeComplain')->name('storeComplain');
Route::get('/getComplains','TController@getComplains')->name('getComplains');
