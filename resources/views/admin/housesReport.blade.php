<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Rooms Report</title>
@include('admin.bts')
</head>
<body>
<div class="container-fluid">
<h3 class="text-center text-warning">ONLINE APARTMENT MANAGEMENT SYSTEM </h3>
<hr>
<h4 class="text-center">ALL APARTMENTS' ROOMS</h4>
<hr>
   <table class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Room No.</th>
                  <th>Type</th>
                  <th>Status</th>
                  <th>Rent Amount</th>
                  <th>Apartment</th>
                </tr>
              </thead>
              <tbody>
              @php 
            $i=0;
              @endphp
              @foreach($houses as $houses)
              @php
              $i +=1;
              @endphp
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$houses->houseID}}</td>
                  <td>{{$houses->size}}</td>
                  <td>{{$houses->status}}</td>
                  <td>{{$houses->rent_amount}}</td>
                  <td>{{$houses->fname}}</td>
                  
              
                </tr>
               @endforeach
              </tbody>
            </table>
                    </div>
</body>
</html>