
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

     <title>ONLINE APARTMENT MANAGEMENT SYSTEM</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('theme/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font awesome CSS -->
    <link href="{{asset('theme/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{asset('theme/css/carousel.css')}}" rel="stylesheet">
     <link href="{{asset('theme/media/css/jquery.dataTables.css')}}" rel="stylesheet">
    <link href="{{asset('theme/css/demo_table_jui.css')}}" rel="stylesheet">
  </head>
<!-- NAVBAR
================================================== -->
  <body>
  
      

        <nav class="navbar navbar-default navbar-fixed-top" style="border-radius:0px;">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{url('/')}}">ONLINE APARTMENT MANAGEMENT SYSTEM</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse ">
              <ul class="nav navbar-nav">
                <li><a href="{{url('/')}}">Home</a></li>

              </ul>
               <ul class="nav navbar-nav navbar-right">
               <li class="active"><a href="{{url('/pages/apartments')}}">Apartments</a></li>
               <li ><a href="{{url('/pages/rooms')}}">Vacant Rooms</a></li>
                
                <li><a href="{{url('/login')}}"><i class="fa fa-fw fa-rocket"></i>Login</a></li>
                 <li><a href=""></a></li>
              

              </ul>
            </div>
          </div>
        </nav>
       <div class="jumbotron" style="height: 150px;">
        <div class="page-header text-center">
  <h3 class="text text-warning">List of all Apartments <small></small></h3>
</div>
        </div>
        <div class="container">
   <div class="table-responsive">
            <table class="table table-striped table-bordered display" id="datatables">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Apartment name</th>
                  <th>Location</th>
                  <th>Name of Landlord</th>
                  <th>Landlord's Phone</th>

                </tr>
              </thead>
              <tbody>
              @php 
            $i=0;
              @endphp
              @foreach($apartments as $apartments)
              @php
              $i +=1;
              @endphp
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$apartments->name}}</td>
                  <td>{{$apartments->location}}</td>
                  <td>{{$apartments->fname}}</td>
                  <td>{{$apartments->phone}}</td>
              
                </tr>
               @endforeach
              </tbody>
            </table>
          </div>

<hr>
      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2017 APARTMENT MANAGEMENT SYSTEM. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  <script src="{{asset('theme/js/jquery.min.js')}}"></script>
    <script src="{{asset('theme/js/bootstrap.min.js')}}"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="{{asset('theme/js/holder.min.js')}}"></script>
    <script src="{{asset('theme/media/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript">
$(document).ready(function(){
  $('#datatables').dataTable({

    "sPaginationType":"full_numbers",
    "aaSorting":[[2,"asc"]],
    "bJQueryUI":true
  });
})
    </script>
  </body>
</html>
