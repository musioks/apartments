@extends('landlord.layout')
@section('pagename') My Rooms @stop
@section('content')
           <div class="table-responsive">
            <table class="table table-striped display" id="datatables">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Room No.</th>
                  <th>Type</th>
                  <th>Status</th>
                  <th>Rent Per month</th>
                  <th>Apartment</th>
                </tr>
              </thead>
              <tbody>
              @php 
            $i=0;
              @endphp
              @foreach($houses as $houses)
              @php
              $i +=1;
              @endphp
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$houses->houseID}}</td>
                  <td>{{$houses->size}}</td>
                  <td>{{$houses->status}}</td>
                  <td>Kshs.{{$houses->rent_amount}}</td>
                  <td>{{$houses->name}}</td>
                  
              
                </tr>
               @endforeach
              </tbody>
            </table>
          </div>
          @stop