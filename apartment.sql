-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 15, 2017 at 10:22 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apartment`
--

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'kVKOocC41frjIlW5IVyvOu0RUI1cz2qm', 1, '2017-05-19 07:45:19', '2017-05-19 07:45:18', '2017-05-19 07:45:19'),
(2, 2, 'tXPNf4zIHJD4L2fWe5y30CqtZtFLthLH', 1, '2017-05-30 16:13:03', '2017-05-30 16:13:03', '2017-05-30 16:13:03'),
(3, 3, 'IZ8moTU3ZJtWzh1h5CbdqNwmBKN8isdW', 1, '2017-06-04 23:15:38', '2017-06-04 23:15:38', '2017-06-04 23:15:38'),
(4, 4, 'XW149l3xQz2nUwhmTRLzDySh8e3y3kIp', 1, '2017-06-05 05:12:01', '2017-06-05 05:12:01', '2017-06-05 05:12:01'),
(5, 6, 'uB15lxu06vfvHgKhR1uUAJgRRjB9lE1w', 1, '2017-06-08 23:48:35', '2017-06-08 23:48:35', '2017-06-08 23:48:35'),
(7, 8, 'NAgGyLpbtMay7s9OtetZuJsxENkcQm49', 1, '2017-06-14 17:18:25', '2017-06-14 17:18:25', '2017-06-14 17:18:25'),
(8, 9, 'MZH4xNuc8rYIOyTIouLcEqhu5MQxBLws', 1, '2017-06-14 17:57:03', '2017-06-14 17:57:03', '2017-06-14 17:57:03'),
(9, 10, 'PNjaU9ePldOWArQyaBWDAun2Kaz9g2O9', 1, '2017-06-14 18:29:34', '2017-06-14 18:29:34', '2017-06-14 18:29:34');

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `title`, `message`, `created_at`, `updated_at`) VALUES
(1, 'New Apartment', 'We have built a new apartment near cherangany hills', '2017-06-15 04:58:42', '2017-06-15 04:58:42');

-- --------------------------------------------------------

--
-- Table structure for table `apartments`
--

CREATE TABLE `apartments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `landlord_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `apartments`
--

INSERT INTO `apartments` (`id`, `name`, `location`, `landlord_id`, `created_at`, `updated_at`) VALUES
(1, 'Belle Apartments', 'Gong RD. near Prestige', 1, '2017-06-05 02:25:52', '2017-06-05 02:25:52'),
(2, 'Moda Apartments', 'Msa rd. at Bellevue', 1, '2017-06-05 02:40:52', '2017-06-05 02:40:52'),
(3, 'Promenade Apartments', 'Yatta District', 1, '2017-06-05 02:42:26', '2017-06-05 02:42:26');

-- --------------------------------------------------------

--
-- Table structure for table `houses`
--

CREATE TABLE `houses` (
  `id` int(10) UNSIGNED NOT NULL,
  `houseID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rent_amount` int(11) NOT NULL,
  `apartment_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `houses`
--

INSERT INTO `houses` (`id`, `houseID`, `size`, `status`, `rent_amount`, `apartment_id`, `created_at`, `updated_at`) VALUES
(1, 'B21', 'Bedsitter', 'Vacant', 8500, 1, '2017-06-05 04:07:06', '2017-06-05 04:07:06'),
(2, 'A34', 'Two Bedroom', 'Vacant', 21000, 2, '2017-06-15 02:58:48', '2017-06-15 02:58:48');

-- --------------------------------------------------------

--
-- Table structure for table `landlords`
--

CREATE TABLE `landlords` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `landlords`
--

INSERT INTO `landlords` (`id`, `user_id`, `name`, `phone`, `email`, `salary`, `created_at`, `updated_at`) VALUES
(1, 3, 'Grace Ogot', '0721875690', 'graceogot@yahoo.com', 45000, '2017-06-04 23:15:39', '2017-06-04 23:15:39'),
(2, 6, 'Jacob Kalinda', '0733456455', 'jacob@yahoo.com', 23000, '2017-06-08 23:48:35', '2017-06-08 23:48:35'),
(4, 8, 'david oguta', '0721875600', 'oguta@david.com', 45000, '2017-06-14 17:18:25', '2017-06-14 17:18:25'),
(5, 9, 'Maurine George', '0700555601', 'maurine@gmail.com', 45000, '2017-06-14 17:57:03', '2017-06-14 17:57:03');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_07_02_230147_migration_cartalyst_sentinel', 1),
(2, '2017_05_18_170331_create_landlords_table', 1),
(3, '2017_05_18_170508_create_apartments_table', 1),
(4, '2017_05_18_170551_create_houses_table', 1),
(5, '2017_05_18_170630_create_tenants_table', 1),
(6, '2017_05_18_170652_create_payments_table', 1),
(7, '2017_06_15_073544_create_announcements_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `tenant_id` int(10) UNSIGNED NOT NULL,
  `payment_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slipNo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `date_paid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(2, 1, 'YLWvpvRgWfSaFv2Obf7kv7PQWq7HwRgH', '2017-06-04 12:24:02', '2017-06-04 12:24:02'),
(3, 1, '4hSGm3Z8KBoMR95lie6bxZRsLahlu3sl', '2017-06-04 22:36:02', '2017-06-04 22:36:02'),
(5, 1, 'LAIWRgiAmvoWD7VCMT2BPO42UBBet4r9', '2017-06-07 04:02:58', '2017-06-07 04:02:58'),
(6, 1, 'V38xhwbQi4yYJb1qpOGy0jGKdnQeBXFR', '2017-06-08 23:47:14', '2017-06-08 23:47:14'),
(7, 1, 'HsAFajCH6hDlnffJexXbIGK3yaWMeA4E', '2017-06-09 03:08:37', '2017-06-09 03:08:37'),
(8, 1, '7UD4hEwjm6pIzPMXy1g0okUB5ZivczZf', '2017-06-09 05:10:04', '2017-06-09 05:10:04'),
(13, 9, '19ygWuPBA0KVDq99MeWakB3yH020brQE', '2017-06-14 17:58:47', '2017-06-14 17:58:47'),
(16, 10, 'e1pIqYsJI7xtWejF9lFdMd6mvqKt8MLG', '2017-06-14 18:29:52', '2017-06-14 18:29:52'),
(17, 10, '0VJhvDtc9NQKHiW33LxIJAu0MXjKtURJ', '2017-06-14 18:35:01', '2017-06-14 18:35:01'),
(20, 1, 'NbuJpD4Cl1HuXjS0xB94sjKYoVHp5mDa', '2017-06-15 04:50:41', '2017-06-15 04:50:41');

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

CREATE TABLE `reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', 'Systems admin', NULL, NULL),
(2, 'lord', 'Landlord', 'Landlord', NULL, NULL),
(3, 'tenant', 'Tenant', 'Tenant', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2017-05-19 07:45:19', '2017-05-19 07:45:19'),
(2, 1, '2017-05-30 16:13:04', '2017-05-30 16:13:04'),
(3, 2, '2017-06-04 23:15:39', '2017-06-04 23:15:39'),
(4, 3, '2017-06-05 05:12:01', '2017-06-05 05:12:01'),
(6, 2, '2017-06-08 23:48:35', '2017-06-08 23:48:35'),
(8, 2, '2017-06-14 17:18:25', '2017-06-14 17:18:25'),
(9, 2, '2017-06-14 17:57:03', '2017-06-14 17:57:03'),
(10, 3, '2017-06-14 18:29:34', '2017-06-14 18:29:34');

-- --------------------------------------------------------

--
-- Table structure for table `tenants`
--

CREATE TABLE `tenants` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `house_id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nok` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tenants`
--

INSERT INTO `tenants` (`id`, `user_id`, `house_id`, `fname`, `phone`, `email`, `address`, `nok`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 'asha kim', '0723456789', 'john@mmu.ac.ke', '454 Malaba', 'Daniel Moi', '2017-06-05 05:12:01', '2017-06-05 05:12:01'),
(2, 10, 1, 'asha kim', '0723456782', 'buzeki@yahoo.com', 'Donholm', 'Daniel Moi', '2017-06-14 18:29:34', '2017-06-14 18:29:34');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `type`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, 'global', NULL, '2017-06-04 12:10:23', '2017-06-04 12:10:23'),
(2, NULL, 'ip', '::1', '2017-06-04 12:10:23', '2017-06-04 12:10:23'),
(3, NULL, 'global', NULL, '2017-06-04 12:10:38', '2017-06-04 12:10:38'),
(4, NULL, 'ip', '::1', '2017-06-04 12:10:38', '2017-06-04 12:10:38'),
(5, 1, 'user', NULL, '2017-06-04 12:10:38', '2017-06-04 12:10:38'),
(6, NULL, 'global', NULL, '2017-06-07 03:21:09', '2017-06-07 03:21:09'),
(7, NULL, 'ip', '::1', '2017-06-07 03:21:09', '2017-06-07 03:21:09'),
(8, 1, 'user', NULL, '2017-06-07 03:21:09', '2017-06-07 03:21:09'),
(9, NULL, 'global', NULL, '2017-06-14 16:57:41', '2017-06-14 16:57:41'),
(10, NULL, 'ip', '::1', '2017-06-14 16:57:41', '2017-06-14 16:57:41'),
(11, 6, 'user', NULL, '2017-06-14 16:57:41', '2017-06-14 16:57:41'),
(12, NULL, 'global', NULL, '2017-06-14 16:57:56', '2017-06-14 16:57:56'),
(13, NULL, 'ip', '::1', '2017-06-14 16:57:56', '2017-06-14 16:57:56'),
(14, 6, 'user', NULL, '2017-06-14 16:57:56', '2017-06-14 16:57:56'),
(15, NULL, 'global', NULL, '2017-06-14 16:59:16', '2017-06-14 16:59:16'),
(16, NULL, 'ip', '::1', '2017-06-14 16:59:16', '2017-06-14 16:59:16'),
(17, 6, 'user', NULL, '2017-06-14 16:59:16', '2017-06-14 16:59:16'),
(18, NULL, 'global', NULL, '2017-06-14 17:00:40', '2017-06-14 17:00:40'),
(19, NULL, 'ip', '::1', '2017-06-14 17:00:40', '2017-06-14 17:00:40'),
(20, 6, 'user', NULL, '2017-06-14 17:00:41', '2017-06-14 17:00:41'),
(21, NULL, 'global', NULL, '2017-06-14 17:01:21', '2017-06-14 17:01:21'),
(22, NULL, 'ip', '::1', '2017-06-14 17:01:21', '2017-06-14 17:01:21'),
(23, 6, 'user', NULL, '2017-06-14 17:01:21', '2017-06-14 17:01:21'),
(24, NULL, 'global', NULL, '2017-06-14 17:01:32', '2017-06-14 17:01:32'),
(25, NULL, 'ip', '::1', '2017-06-14 17:01:32', '2017-06-14 17:01:32'),
(26, 1, 'user', NULL, '2017-06-14 17:01:32', '2017-06-14 17:01:32'),
(27, NULL, 'global', NULL, '2017-06-14 17:19:00', '2017-06-14 17:19:00'),
(28, NULL, 'ip', '::1', '2017-06-14 17:19:00', '2017-06-14 17:19:00'),
(29, 8, 'user', NULL, '2017-06-14 17:19:00', '2017-06-14 17:19:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `email`, `password`, `permissions`, `last_login`, `created_at`, `updated_at`) VALUES
(1, 'Josco Njole', '0733456455', 'jmaithya@gmail.com', '$2y$10$8o0FYDXCyYGkKU.lh557Ae/zIVws3JKOT7hvCcSVUGqAocMGIaaWS', NULL, '2017-06-15 04:50:41', '2017-05-19 07:45:18', '2017-06-15 04:50:41'),
(2, 'John Doe', '0723456789', 'myk2030@gmail.com', '$2y$10$.q11YLplsu4g53bJNkdmWuZcO11jbtL1TYYoK345tusZBsA8SGavu', NULL, NULL, '2017-05-30 16:13:03', '2017-05-30 16:13:03'),
(3, 'Grace Ogot', '0721875690', 'graceogot@yahoo.com', '$2y$10$36h9uLGpx6INZIKFC8Yks.vVUxIZUMMWl5lHbb.xAyQEEnf6/SPsu', NULL, NULL, '2017-06-04 23:15:38', '2017-06-04 23:15:38'),
(4, 'asha kim', '0723456789', 'john@mmu.ac.ke', '$2y$10$WQ6s22Tdg4kcoCIGmUAOD.QCX//8nMbWqHv8vZhu0weasriDOdeNC', NULL, NULL, '2017-06-05 05:12:01', '2017-06-05 05:12:01'),
(6, 'Jacob Kalinda', '0733456455', 'jacob@yahoo.com', '$2y$10$c6KYorV1YtbQ0VGy4B5/XOmwx85JjxdHa91kvkMFNh6rq6ANTQ/Jq', NULL, NULL, '2017-06-08 23:48:35', '2017-06-08 23:48:35'),
(8, 'david oguta', '0721875600', 'oguta@david.com', '$2y$10$qizMrnT3KUZ8eImPKYTcaOaf7zAeLsXsUraPr9GPWa5QBOWDz3zve', NULL, NULL, '2017-06-14 17:18:25', '2017-06-14 17:18:25'),
(9, 'Maurine George', '0700555601', 'maurine@gmail.com', '$2y$10$VJHP6VcuALG1GIxEWViWNuaqJxUUhCYxum45RH9z6nl66qaqOONMy', NULL, '2017-06-14 18:09:46', '2017-06-14 17:57:03', '2017-06-14 18:09:46'),
(10, 'asha kim', '0723456782', 'buzeki@yahoo.com', '$2y$10$X9HB6KDwuBns0uZY/l.hNO5Fqvdgz5uxAzQ5fyMINKl9vSv9kEcvC', NULL, '2017-06-14 18:35:02', '2017-06-14 18:29:34', '2017-06-14 18:35:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apartments`
--
ALTER TABLE `apartments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `apartments_name_unique` (`name`),
  ADD KEY `apartments_landlord_id_foreign` (`landlord_id`);

--
-- Indexes for table `houses`
--
ALTER TABLE `houses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `houses_apartment_id_foreign` (`apartment_id`);

--
-- Indexes for table `landlords`
--
ALTER TABLE `landlords`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `landlords_phone_unique` (`phone`),
  ADD UNIQUE KEY `landlords_email_unique` (`email`),
  ADD KEY `landlords_user_id_foreign` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_tenant_id_foreign` (`tenant_id`);

--
-- Indexes for table `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`);

--
-- Indexes for table `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Indexes for table `tenants`
--
ALTER TABLE `tenants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tenants_phone_unique` (`phone`),
  ADD UNIQUE KEY `tenants_email_unique` (`email`),
  ADD KEY `tenants_user_id_foreign` (`user_id`),
  ADD KEY `tenants_house_id_foreign` (`house_id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `apartments`
--
ALTER TABLE `apartments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `houses`
--
ALTER TABLE `houses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `landlords`
--
ALTER TABLE `landlords`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `persistences`
--
ALTER TABLE `persistences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tenants`
--
ALTER TABLE `tenants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `apartments`
--
ALTER TABLE `apartments`
  ADD CONSTRAINT `apartments_landlord_id_foreign` FOREIGN KEY (`landlord_id`) REFERENCES `landlords` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `houses`
--
ALTER TABLE `houses`
  ADD CONSTRAINT `houses_apartment_id_foreign` FOREIGN KEY (`apartment_id`) REFERENCES `apartments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `landlords`
--
ALTER TABLE `landlords`
  ADD CONSTRAINT `landlords_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_tenant_id_foreign` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tenants`
--
ALTER TABLE `tenants`
  ADD CONSTRAINT `tenants_house_id_foreign` FOREIGN KEY (`house_id`) REFERENCES `houses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tenants_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
