@extends('admin.layout')
@section('pagename') Generate Reports @stop
@section('content')
       <div class="row">
            <div class="col-sm-6">
<a href="{{url('/landlordsReport')}}" class="btn btn-primary btn-lg btn-block">
  Landlords <span class="badge">{{count($landy)}}</span>
</a>
            </div>
              <div class="col-sm-6">
<a href="{{url('/apartmentsReport')}}" class="btn btn-warning btn-lg btn-block">
  Apartments <span class="badge">{{count($apartments)}}</span>
</a>
            </div>
         
          </div>
<div class="page-header">&nbsp;</div>
 <div class="row">
              <div class=" col-sm-6">

<a href="{{url('/housesReport')}}" class="btn btn-info btn-lg btn-block" >
  Houses <span class="badge">{{count($houses)}}</span>
</a>
            </div>
              <div class=" col-sm-6">

<a href="{{url('/tenantsReport')}}" class="btn btn-success btn-lg btn-block">
  Tenants <span class="badge">{{count($tenants)}}</span>
</a>
            </div>
         
          </div>


         
          @stop