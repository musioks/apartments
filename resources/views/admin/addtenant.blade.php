@extends('admin.layout')
@section('pagename') Add Tenant @stop
@section('content')
          <form  enctype="multipart/form-data" method="post" action="{{url('/addtenant')}}">
                {{csrf_field()}}
                <div class="box-body">
                 <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="form-group">
                        @if(Session::has('error'))
                            <div class="alert alert-danger text-center">
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success text-center">
                                {{Session::get('success')}}
                            </div>
                        @endif
                    </div>
                </div>
                </div><!--end row-->
                <div class="row">
                    <div class="col-sm-4">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('house_id') ? 'has-error':''}}">
                        <label>Room No.*</label>
                        <select name="house_id" class="form-control" required>
                        <option value="">-----Select Room------</option>
                         @foreach($houses as $houses)
                         <option value="{{$houses->id}}">{{$houses->houseID}}|{{$houses->fname}}</option>
                         @endforeach
                        </select>
                       @if($errors->has('house_id'))
              <span class="help-block">{{$errors->first('house_id')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
             <div class="col-md-4">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('fname') ? 'has-error':''}}">
                        <label>Tenant's Name *</label>
                        <input type="text" name="fname" class="form-control" placeholder="eg. George Mberia">
                        @if($errors->has('fname'))
              <span class="help-block">{{$errors->first('fname')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
               <div class="col-md-4">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('phone') ? 'has-error':''}}">
                        <label>Phone No. *</label>
                        <input type="text" name="phone" class="form-control" placeholder="eg.0798908990">
                        @if($errors->has('phone'))
              <span class="help-block">{{$errors->first('phone')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                 
                </div><!--end row--> 

                <div class="row">
                    <div class="col-md-3">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('email') ? 'has-error':''}}">
                        <label>Email Address *</label>
                        <input type="email" name="email" class="form-control" placeholder="eg. john@mmu.ac.ke">
                        @if($errors->has('email'))
              <span class="help-block">{{$errors->first('email')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                     <div class="col-md-3">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('address') ? 'has-error':''}}">
                        <label>Postal Address *</label>
                        <input type="text" name="address" class="form-control" placeholder="eg. 454 Malaba">
                        @if($errors->has('address'))
              <span class="help-block">{{$errors->first('address')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                 <div class="col-md-3">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('nok') ? 'has-error':''}}">
                        <label>Next of Kin*</label>
                        <input type="text" name="nok" class="form-control" placeholder="Next of Kin">
                        @if($errors->has('nok'))
              <span class="help-block">{{$errors->first('nok')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                 <div class="col-md-3">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('password') ? 'has-error':''}}">
                        <label>Password*</label>
                        <input type="password" name="password" class="form-control" placeholder="e.g tenant123">
                        @if($errors->has('password'))
              <span class="help-block">{{$errors->first('password')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                     
                </div><!--end row-->  

                <div class="row">                
                <div class="col-md-6 col-md-offset-5">

                    <div class="form-group">

                        <button type="submit" class="btn btn-success" >Add Tenant</button>
                    </div>
                </div>

                <!-- /.form-group -->
                     </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
            </form>
          @stop