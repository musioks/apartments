@extends('landlord.layout')
@section('pagename') Announcements @stop
@section('content')
           <div class="table-responsive">
            <table class="table table-striped display" id="datatables">
              <thead>
                <tr>
                  <th >#</th>
                  <th>Title</th>
                  <th>Announcement</th>
                  <th>Date Made</th>
                </tr>
              </thead>
              <tbody>
              @php 
            $i=0;
              @endphp
              @foreach($notices as $notices)
              @php
              $i +=1;
              @endphp
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$notices->title}}</td>
                  <td>{{$notices->message}}</td>
                  <td>{{date('F d, Y', strtotime($notices->created_at))}}</td>
                  
                              
                </tr>
               @endforeach
              </tbody>
            </table>
          </div>
          @stop