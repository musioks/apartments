@extends('admin.layout')
@section('pagename') View/Edit Apartment @stop
@section('content')
          <form  enctype="multipart/form-data" method="post" action="{{url('/editApartment/'.$apartments->id)}}">
                {{csrf_field()}}
                <div class="box-body">
                 <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="form-group">
                        @if(Session::has('error'))
                            <div class="alert alert-danger text-center">
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success text-center">
                                {{Session::get('success')}}
                            </div>
                        @endif
                    </div>
                </div>
                </div><!--end row-->
                <div class="row">
                       <div class="col-md-6">
                <!-- /.form-group -->
                    <div class="form-group">
                        <label>Apartment Name *</label>
                        <input type="text" name="name" class="form-control" value="{{$apartments->name}}">
                    </div>
                <!-- /.form-group -->
                </div>
                <div class="col-sm-6">
                 <!-- /.form-group -->
                    <div class="form-group">
                        <label> Where is it located? *</label>
                        <input type="text" name="location" class="form-control" value="{{$apartments->location}}">
                    </div>
                <!-- /.form-group -->
                </div>
                
                 
                </div><!--end row--> 

                <div class="row">
                  <div class="col-sm-12">
                 <!-- /.form-group -->
                    <div class="form-group">
                        <label> Landlord in charge</label>
                        <select name="landlord_id" class="form-control">
                        <option selected="{{$apartments->lordID}}">{{$apartments->fname}}</option>   
                         @foreach($lords as $lords)
                         <option value="{{$lords->id}}">{{$lords->name}}</option>
                         @endforeach
                        </select>
                       
                    </div>
                <!-- /.form-group -->
                </div>
                     
                </div><!--end row-->  

                <div class="row">                
                <div class="col-md-6 col-md-offset-5">

                    <div class="form-group">

                        <button type="submit" class="btn btn-warning" >Update Apartment</button>
                    </div>
                </div>

                <!-- /.form-group -->
                     </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
            </form>
          @stop