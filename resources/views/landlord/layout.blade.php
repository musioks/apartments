
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>ONLINE APARTMENT MANAGEMENT SYSTEM</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('theme/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font awesome CSS -->
    <link href="{{asset('theme/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{asset('theme/css/dashboard.css')}}" rel="stylesheet">
     <link href="{{asset('theme/media/css/jquery.dataTables.css')}}" rel="stylesheet">
    <link href="{{asset('theme/css/demo_table_jui.css')}}" rel="stylesheet">
  </head>

  <body>

    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{route('landlord.index')}}">ONLINE APARTMENT MANAGEMENT SYSTEM</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{{route('landlord.index')}}"><i class="fa fa-fw fa-dashboard"></i>Dashboard</a></li>
            <li><a href="#"><i class="fa fa-fw fa-user"></i>{{Sentinel::getUser()->name}}</a></li>
            <li> <a  href="{{ route('signout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();"><i class="fa fa-power-off fa-fw"></i>Sign Out</a>
                         <form id="logout-form" action="{{ route('signout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                             </form></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="{{route('landlord.index')}}"><i class="fa fa-fw fa-dashboard"></i>Dashboard <span class="sr-only">(current)</span></a></li>
             <li><a href="{{route('myapartments')}}"><i class="fa fa-fw fa-building"></i> My Apartments</a></li>
             <li><a href="{{route('myrooms')}}"><i class="fa fa-fw fa-home"></i> My Rooms</a></li>
             <li><a href="{{route('mytenants')}}"><i class="fa fa-fw fa-users"></i> My Tenants</a></li>
             <li><a href="{{route('notifications')}}"><i class="fa fa-fw fa-info"></i>Announcements</a></li>
             <li><a href="{{route('complains')}}"><i class="fa fa-fw fa-bullhorn"></i>Complains</a></li>
             <li><a href="{{route('maintenance')}}"><i class="fa fa-fw fa-wrench"></i>Maintenance Requests</a></li>
             <li><a href=""><i class="fa fa-fw fa-money"></i> View Payments</a></li>
            
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header"> @yield('pagename') </h1>
@yield('content')
          
        </div><!-- -->
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{asset('theme/js/jquery.min.js')}}"></script>
    <script src="{{asset('theme/js/bootstrap.min.js')}}"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="{{asset('theme/js/holder.min.js')}}"></script>
        <script src="{{asset('theme/media/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript">
$(document).ready(function(){
  $('#datatables').dataTable({

    "sPaginationType":"full_numbers",
    "aaSorting":[[2,"asc"]],
    "bJQueryUI":true
  });
})
    </script>
  </body>
</html>
