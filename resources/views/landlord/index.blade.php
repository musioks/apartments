@extends('landlord.layout')
@section('pagename') Landlord Dashboard @stop
@section('content')
<hr>
<div class="row ">
              <div class="col-xs-6 col-sm-4">
<a href="{{route('myapartments')}}" class="btn btn-warning btn-lg">
  My Apartments <span class="badge">{{count($apartments)}}</span>
</a>
            </div>
              <div class="col-xs-6 col-sm-4 placeholder">

<a href="{{route('myrooms')}}" class="btn btn-info btn-lg" >
  Rooms <span class="badge">{{count($houses)}}</span>
</a>
            </div>
              <div class="col-xs-6 col-sm-4 placeholder">

<a href="{{route('mytenants')}}" class="btn btn-success btn-lg" >
  My Tenants <span class="badge">{{count($tenants)}}</span>
</a>
            </div>
         
          </div>
 @stop