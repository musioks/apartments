@extends('tenants.layout')
@section('pagename') Complains @stop
@section('content')
           <div class="table-responsive">
            <table class="table table-striped display" id="datatables">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Complaint Title</th>
                  <th>Complaint Description</th>
                  <th>Complaint Status</th>
                  <th>Date Posted</th>
                </tr>
              </thead>
              <tbody>
              @php 
            $i=0;
              @endphp
              @foreach($data as $data)
              @php
              $i +=1;
              @endphp
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$data->about}}</td>
                  <td>{{$data->description}}</td>
                  <td>
                  @if($data->status==0)
                  {{"Unattended"}}
               @else
               {{"Attended"}}
               @endif
                  </td>
                  <td>{{date('F d, Y', strtotime($data->created_at))}}</td>
                </tr>
               @endforeach
              </tbody>
            </table>
          </div>
          @stop