<?php

namespace App\Http\Controllers;

use App\House; 
use App\Apartment; 
use DB;
use PDF;
use Illuminate\Http\Request;

class HouseController extends Controller
{
    public function __construct(){
        $this->middleware('admin');
    }
     public function houses()
    {
      $houses=DB::table('houses')->join('apartments','houses.apartment_id','=','apartments.id')
    ->select('houses.*','apartments.name as fname','apartments.location')
    ->get();
      return view('admin.houses',['houses'=>$houses]); 
    }
    public function addhouse()
    {
        $apartments=Apartment::all();
    return view('admin.addhouse',['apartments'=>$apartments]);
    }

    public function storeHouse(Request $request)
    {
         $this->validate($request,[
        'houseID'=>'required|unique:houses',
        'size'=>'required',
        'status'=>'required',
        'rent_amount'=>'required',
        ]);
         $house=new House();
         $house->houseID=$request->houseID;
         $house->size=$request->size;
         $house->status=$request->status;
         $house->rent_amount=$request->rent_amount;
         $house->apartment_id=$request->apartment_id;
         $house->save();
         return redirect()->back()->with('success','Houses created successfully!');
    }
     public function housesReport(){
            $houses=DB::table('houses')->join('apartments','houses.apartment_id','=','apartments.id')
    ->select('houses.*','apartments.name as fname','apartments.location')
    ->get();
            $pdf=PDF::loadView('admin.housesReport',['houses'=>$houses])->setPaper('a4', 'landscape');
            return $pdf->stream('houses.pdf');
   }
    public function editHouse($id)
    {
        //
    }

    public function updateHouse(Request $request,$id)
    {
        //
    }

    public function deleteHouse($id)
    {
        //
    }
}
