<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Tenants Report</title>
@include('admin.bts')
</head>
<body>
<div class="container-fluid">
<h3 class="text-center text-warning">ONLINE APARTMENT MANAGEMENT SYSTEM </h3>
<hr>
<h4 class="text-center">ALL TENANTS</h4>
<hr>
   <table class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Full Name</th>
                  <th>Phone No.</th>
                  <th>Email Address</th>
                  <th>Postal Address</th>
                  <th>Next of Kin</th>
                  <th>House No.</th>
                  <th>Apartment Name</th>
                </tr>
              </thead>
              <tbody>
              @php 
            $i=0;
              @endphp
              @foreach($tenants as $tenants)
              @php
              $i +=1;
              @endphp
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$tenants->fname}}</td>
                  <td>{{$tenants->phone}}</td>
                  <td>{{$tenants->email}}</td>
                  <td>{{$tenants->address}}</td>
                  <td>{{$tenants->nok}}</td>
                  <td>{{$tenants->houseID}}</td>
                  <td>{{$tenants->name}}</td>
              
                  
              
                </tr>
               @endforeach
              </tbody>
            </table>
                    </div>
</body>
</html>