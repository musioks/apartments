<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Session;
class LoginController extends Controller
{
	public function __construct(){
      $this->middleware('guest', ['except' => 'getLogout']);
    }
	public function login(){
    return view('login');
	}

public function Sign_In(Request $request){
        $this->validate($request,[
            'email'=>'required',
            'password'=>'required',
        ]);
 
     if(Sentinel::authenticate($request->all())){
         $slug= Sentinel::getUser()->roles()->first()->slug;
         if($slug=='admin'){
             return redirect()->route('admin.index');
         }
         elseif($slug=='lord'){
             return redirect()->route('landlord.index');
         }
          elseif($slug=='tenant'){
             return redirect()->route('tenant.index');
         }
       
     }
     else{
         return redirect()->back()->with('error','wrong credentials!');
     }
 

    }


     public function getLogout()
    {
     Sentinel::logout();
        return redirect('/');

    }
}
