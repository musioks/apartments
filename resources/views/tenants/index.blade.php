@extends('tenants.layout')
@section('pagename') Tenant Dashboard @stop
@section('content')
<div class="panel panel-info">
  <!-- Default panel contents -->
  <div class="panel-heading text-center">My Info</div>
  <div class="panel-body">
  <div class="page-header">
  <h2 class="text-center">Name: <strong>{{$data->fname}}</strong></h2>
</div>
  </div>
<div class="row">
<div class="col-sm-6">
  <!-- List group -->
  <ul class="list-group">
    <li class="list-group-item">Email: <strong>{{$data->email}}</strong></li>
    <li class="list-group-item">Address: <strong>{{$data->address}}</strong></li>
    <li class="list-group-item">Phone No: <strong>{{$data->phone}}</strong></li>
    <li class="list-group-item">Next of Kin: <strong>{{$data->nok}}</strong></li>
  </ul>
  </div>
  <div class="col-sm-6">
  <!-- List group -->
  <ul class="list-group">
  <li class="list-group-item">Room Number: <strong>{{$data->houseID}}</strong></li>
  <li class="list-group-item">Rent Per Month: <strong>Kshs.{{$data->rent_amount}}</strong></li>
    <li class="list-group-item">Apartment Name: <strong>{{$data->name}}</strong></li>
    <li class="list-group-item">Apartment Location: <strong>{{$data->location}}</strong></li>
  </ul>
  </div>
  </div>
</div>

          
          @stop