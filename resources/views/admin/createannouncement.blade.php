@extends('admin.layout')
@section('pagename') Announcements @stop
@section('content')
          <form  enctype="multipart/form-data" method="post" action="{{url('/createannouncement')}}">
                {{csrf_field()}}
                <div class="box-body">
                 <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="form-group">
                        @if(Session::has('error'))
                            <div class="alert alert-danger text-center">
                              <i class="fa fa-fw fa-window-close"></i>  {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success text-center">
                            <i class="fa fa-fw fa-check"></i> 
                                {{Session::get('success')}}
                            </div>
                        @endif
                    </div>
                </div>
                </div><!--end row-->
                <div class="row">
                       <div class="col-md-6">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('title') ? 'has-error':''}}">
                        <label>Title *</label>
                        <input type="text" name="title" class="form-control" placeholder="eg. New Apartment">
                        @if($errors->has('title'))
              <span class="help-block">{{$errors->first('title')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <div class="col-sm-6">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('message') ? 'has-error':''}}">
                        <label> Announcement.*</label>
                        <textarea name="message" class="form-control" >
                        </textarea>
                        @if($errors->has('message'))
              <span class="help-block">{{$errors->first('message')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                
                 
                </div><!--end row--> 

               

                <div class="row">                
                <div class="col-md-6 col-md-offset-5">

                    <div class="form-group">

                        <button type="submit" class="btn btn-success" >Create Announcement</button>
                    </div>
                </div>

                <!-- /.form-group -->
                     </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
            </form>
          @stop