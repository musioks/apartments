@extends('landlord.layout')
@section('pagename') Maintenance Requests @stop
@section('content')
           <div class="table-responsive">
            <table class="table table-striped display" id="datatables">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Tenant Name</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Room No.</th>
                  <th>Apartment</th>
                  <th>Request Title</th>
                  <th>Description</th>
                  <th>Status</th>
                  <th>Date Posted</th>
                </tr>
              </thead>
              <tbody>
              @php 
            $i=0;
              @endphp
              @foreach($data as $data)
              @php
              $i +=1;
              @endphp
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$data->fname}}</td>
                  <td>{{$data->phone}}</td>
                  <td>{{$data->email}}</td>
                  <td>{{$data->houseID}}</td>
                  <td>{{$data->name}}</td>
                  <td>{{$data->title}}</td>
                  <td>{{$data->description}}</td>
                  <td>@if($data->status==0)
                  {{"Unattended"}}
               @else
               {{"Attended"}}
               @endif</td>
                  <td>{{date('F d, Y', strtotime($data->created_at))}}</td>
                </tr>
               @endforeach
              </tbody>
            </table>
          </div>
          @stop