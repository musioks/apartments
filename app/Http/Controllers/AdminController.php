<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AdminController extends Controller
{
	public function __construct(){
		$this->middleware('admin');
	}
    public function index(){
    	$landy=\App\Landlord::all();
    	$houses=\App\House::all();
    	$tenants=\App\Tenant::all();
    	$apartments=\App\Apartment::all();
    	return view('admin.index',['landy'=>$landy,'houses'=>$houses,'tenants'=>$tenants,'apartments'=>$apartments]);
    }
     public function reports(){
        $landy=\App\Landlord::all();
        $houses=\App\House::all();
        $tenants=\App\Tenant::all();
        $apartments=\App\Apartment::all();
    	return view('admin.reports',['landy'=>$landy,'houses'=>$houses,'tenants'=>$tenants,'apartments'=>$apartments]);
    }
     public function payments(){
    	return view('admin.payments');
    }
}
