@extends('admin.layout')
@section('pagename') View all Landlords @stop
@section('content')
           <div class="table-responsive">
            <table class="table table-striped display" id="datatables">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Full Name</th>
                  <th>Phone</th>
                  <th>Email Address</th>
                  <th>Salary</th>
                </tr>
              </thead>
              <tbody>
              @php 
            $i=0;
              @endphp
              @foreach($lords as $lords)
              @php
              $i +=1;
              @endphp
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$lords->name}}</td>
                  <td>{{$lords->phone}}</td>
                  <td>{{$lords->email}}</td>
                  <td>Kshs.{{$lords->salary}}</td>
              
                </tr>
               @endforeach
              </tbody>
            </table>
          </div>
          @stop