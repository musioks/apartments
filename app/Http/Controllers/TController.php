<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use DB;
class TController extends Controller
{
 
  public function __construct(){
      $this->middleware('tenant');
    }

    public function index()
    {
    $user=Sentinel::getUser();
    $data=DB::table('tenants')->join('houses','tenants.house_id','=','houses.id')
     ->join('apartments','houses.apartment_id','=','apartments.id')
    ->select('tenants.*','houses.houseID','houses.size','houses.rent_amount',
        'apartments.name','apartments.location')
    ->where('tenants.email','=',$user->email)
    ->first();
        return view('tenants.index',['data'=>$data]);
    }

 
    public function notices()
    {
         $notices=\App\Announcement::all();
        return view('tenants.notices',['notices'=>$notices]);
        
    }

////////////////////////////////////Requests////////////////////////////////////////////////////////

    public function createRequest()
    {
         $user=Sentinel::getUser();
    $data=DB::table('tenants')->join('houses','tenants.house_id','=','houses.id')
     ->join('apartments','houses.apartment_id','=','apartments.id')
    ->select('tenants.*','houses.houseID','houses.size','houses.rent_amount',
        'apartments.name','apartments.location')
    ->where('tenants.email','=',$user->email)
    ->first();
        return view('tenants.createRequest',['data'=>$data]);
        
    }
    public function storeRequest(Request $request)
    {
        $this->validate($request,[
'title'=>'required',
'description'=>'required'
    ]);
$r=new \App\Maintenance();
$r->tenant_id=$request->tenant_id;
$r->title=$request->title;
$r->description=$request->description;
$r->save();

return redirect()->back()->with('success','Maintenance request posted successfully!');
        
    }
    public function getRequests()
    {
         $user=Sentinel::getUser();
  $data=DB::table('maintenances')->join('tenants','maintenances.tenant_id','=','tenants.id')
     ->join('houses','tenants.house_id','=','houses.id')
     ->join('apartments','houses.apartment_id','=','apartments.id')
    ->select('maintenances.*','tenants.fname','tenants.phone','tenants.email','houses.houseID',
        'apartments.name','apartments.location')
    ->where('tenants.email','=',$user->email)
    ->get();
        return view('tenants.getRequests',['data'=>$data]);
    }
    ////////////////////////////////////Complains/////////////////////////////////////

      public function postComplain()
    {
         $user=Sentinel::getUser();
    $data=DB::table('tenants')->join('houses','tenants.house_id','=','houses.id')
     ->join('apartments','houses.apartment_id','=','apartments.id')
    ->select('tenants.*','houses.houseID','houses.size','houses.rent_amount',
        'apartments.name','apartments.location')
    ->where('tenants.email','=',$user->email)
    ->first();
        return view('tenants.postComplain',['data'=>$data]);
        
    }
    public function storeComplain(Request $request)
    {
        $this->validate($request,[
'about'=>'required',
'description'=>'required'
    ]);
$r=new \App\Complain();
$r->tenant_id=$request->tenant_id;
$r->about=$request->about;
$r->description=$request->description;
$r->save();

return redirect()->back()->with('success','Complaint posted successfully!');
        
    }
    public function getComplains()
    {
            $user=Sentinel::getUser();
        $data=DB::table('complains')->join('tenants','complains.tenant_id','=','tenants.id')
     ->join('houses','tenants.house_id','=','houses.id')
     ->join('apartments','houses.apartment_id','=','apartments.id')
    ->select('complains.*','tenants.fname','tenants.phone','tenants.email','houses.houseID',
        'apartments.name','apartments.location')
    ->where('tenants.email','=',$user->email)
    ->get();
        return view('tenants.getComplains',['data'=>$data]);
    }
}
