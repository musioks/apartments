<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\Landlord;
use DB;
use PDF;
use Illuminate\Http\Request;

class ApartmentController extends Controller
{
    public function __construct(){
        $this->middleware('admin');
    }
   
    public function apartments()
    {
        
    $apartments=DB::table('apartments')->join('landlords','apartments.landlord_id','=','landlords.id')
    ->select('apartments.*','landlords.name as fname','landlords.email')
    ->get();
      return view('admin.apartments',['apartments'=>$apartments]);
    }

    public function addapartment()
    {
      $lords=Landlord::all();
        return view('admin.addapartment',['lords'=>$lords]);
    }
    public function storeApartment(Request $request)
    {
        
        $this->validate($request,[
        'name'=>'required',
        'location'=>'required',
            ]);
        $apartment=new Apartment();
        $apartment->name=$request->name;
        $apartment->location=$request->location;
        $apartment->landlord_id=$request->landlord_id;
        $apartment->save();
        return redirect()->back()->with('success','Apartment added successfully!');
    }
 public function apartmentsReport(){
           $apartments=DB::table('apartments')->join('landlords','apartments.landlord_id','=','landlords.id')
    ->select('apartments.*','landlords.name as fname','landlords.email')
    ->get();
            $pdf=PDF::loadView('admin.apartmentsReport',['apartments'=>$apartments])->setPaper('a4', 'landscape');
            return $pdf->stream('apartments.pdf');
   }
 
  
    public function editApartment($id)
    {
       
     $apartments=DB::table('apartments')->join('landlords','apartments.landlord_id','=','landlords.id')
    ->select('apartments.*','landlords.name as fname','landlords.email','landlords.id as lordID')
    ->where('apartments.id','=',$id)
    ->first();
    $lords=\App\Landlord::all();
      return view('admin.editApartment',['apartments'=>$apartments,'lords'=>$lords]); 
    }
  
    public function updateApartment(Request $request,$id)
    {
        DB::table('apartments')
            ->where('id', $id)
            ->update(
                [
                'name' => $request->name,
                'location' => $request->location,
                'landlord_id' => $request->landlord_id
                ]
                );
            return redirect()->back()->with('success','Apartment details updated successfully!');

    }

   
    public function destroy(Apartment $apartment)
    {
        //
    }
}
