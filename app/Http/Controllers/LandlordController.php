<?php

namespace App\Http\Controllers;

use App\Landlord;
use DB;
use PDF;
use Sentinel;
use Illuminate\Http\Request;

class LandlordController extends Controller
{
 public function __construct(){
      $this->middleware('admin');
    }
    public function landlords()
    {
    $lords=Landlord::all();
    return view('admin.landlords',['lords'=>$lords]);
    }
    public function addlandlord()
    {
    return view('admin.addlandlord');
    }
    public function storeLandlord(Request $request)
    {
        $this->validate($request,[
        'name'=>'required',
        'email'=>'required|unique:landlords',
        'phone'=>'required|unique:landlords',
        'salary'=>'required',
        'password'=>'required',
            ]);
            DB::transaction(function() use($request){
            $credentials=[
            'name'=>$request->name,
            'phone'=>$request->phone,
            'email'=>$request->email,
            'password'=>$request->password,

            ];
       $user=Sentinel::registerAndActivate($credentials);
        $role=Sentinel::findRoleBySlug('lord');
        $role->users()->attach($user);
                $landy=new Landlord();
       
           $landy->user_id=$user->id;
           $landy->name=$request->name;
           $landy->phone=$request->phone;
           $landy->email=$request->email;
           $landy->salary=$request->salary;
           $landy->save();
           
     });
        return redirect()->back()->with('success','Landlord Added Successfully!');
    }
  public function landlordsReport(){
            $lords=Landlord::all();
            $pdf=PDF::loadView('admin.landlordsReport',['lords'=>$lords])->setPaper('a4', 'landscape');
            return $pdf->stream('landlords.pdf');
   }
    public function editLandlord($id)
    {
        //
    }
    public function updateLandlord(Request $request,$id)
    {
        //
    }
    public function deleteLandlord($id)
    {
        //
    }
}
