
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>ONLINE APARTMENT MANAGEMENT SYSTEM</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('theme/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font awesome CSS -->
    <link href="{{asset('theme/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{asset('theme/css/dashboard.css')}}" rel="stylesheet">
     <link href="{{asset('theme/media/css/jquery.dataTables.css')}}" rel="stylesheet">
    <link href="{{asset('theme/css/demo_table_jui.css')}}" rel="stylesheet">
  </head>

  <body>

    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{route('admin.index')}}">ONLINE APARTMENT MANAGEMENT SYSTEM</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{{route('admin.index')}}"><i class="fa fa-fw fa-dashboard"></i>Dashboard</a></li>
            <li><a href="#"><i class="fa fa-fw fa-user"></i>{{Sentinel::getUser()->name}}</a></li>
            <li> <a  href="{{ route('signout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();"><i class="fa fa-power-off fa-fw"></i>Sign Out</a>
                         <form id="logout-form" action="{{ route('signout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                             </form></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="{{route('admin.index')}}"><i class="fa fa-fw fa-dashboard"></i>Dashboard <span class="sr-only">(current)</span></a></li>
            <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="fa fa-fw fa-bars"></i>Landlords<span class="caret"></span></a>
          <ul class="dropdown-menu" style="margin-left:30px;">
            <li><a href="{{route('addlandlord')}}"><i class="fa fa-fw fa-plus"></i>Add a Landlord </a></li>
            <li role="separator" class="divider"></li>
             <li><a href="{{url('/landlords')}}"><i class="fa fa-fw fa-th"></i>View Landlord </a></li>
          </ul>
        </li>
         <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="fa fa-fw fa-university"></i>Apartments<span class="caret"></span></a>
          <ul class="dropdown-menu" style="margin-left:30px;">
            <li><a href="{{route('addapartment')}}"><i class="fa fa-fw fa-plus"></i>Add an Apartment</a></li>
            <li role="separator" class="divider"></li>
             <li><a href="{{route('apartments')}}"><i class="fa fa-fw fa-th"></i>View Apartments</a></li>
          </ul>
        </li>
            <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="fa fa-fw fa-home"></i>Rooms<span class="caret"></span></a>
          <ul class="dropdown-menu" style="margin-left:30px;">
            <li><a href="{{route('addhouse')}}"><i class="fa fa-fw fa-plus"></i>Add a Room</a></li>
            <li role="separator" class="divider"></li>
             <li><a href="{{route('houses')}}"><i class="fa fa-fw fa-th"></i>View Rooms</a></li>
          </ul>
        </li>
           <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="fa fa-fw fa-users"></i>Tenants<span class="caret"></span></a>
          <ul class="dropdown-menu" style="margin-left:30px;">
            <li><a href="{{route('addtenant')}}"><i class="fa fa-fw fa-plus"></i>Add a Tenant</a></li>
            <li role="separator" class="divider"></li>
             <li><a href="{{route('tenants')}}"><i class="fa fa-fw fa-th"></i>View Tenants</a></li>
          </ul>
        </li>
             <li><a href="{{route('payments')}}"><i class="fa fa-fw fa-money"></i> Payments</a></li>
            <li><a href="{{route('reports')}}"><i class="fa fa-fw fa-file"></i>Reports</a></li>
            <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="fa fa-fw fa-info"></i>Announcements<span class="caret"></span></a>
          <ul class="dropdown-menu" style="margin-left:30px;">
            <li><a href="{{route('createannouncement')}}"><i class="fa fa-fw fa-plus"></i>Announce</a></li>
            <li role="separator" class="divider"></li>
             <li><a href="{{route('announcements')}}"><i class="fa fa-fw fa-th"></i>Announcements</a></li>
          </ul>
        </li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header"> @yield('pagename') </h1>
@yield('content')
          
        </div><!-- -->
      </div>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{asset('theme/js/jquery.min.js')}}"></script>
    <script src="{{asset('theme/js/bootstrap.min.js')}}"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="{{asset('theme/js/holder.min.js')}}"></script>
        <script src="{{asset('theme/media/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript">
$(document).ready(function(){
  $('#datatables').dataTable({

    "sPaginationType":"full_numbers",
    "aaSorting":[[2,"asc"]],
    "bJQueryUI":true
  });
})
    </script>
  </body>
</html>
