@extends('tenants.layout')
@section('pagename') Post Complain @stop
@section('content')
          <form  enctype="multipart/form-data" method="post" action="{{url('/postComplain')}}">
                {{csrf_field()}}
                <div class="box-body">
                 <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="form-group">
                        @if(Session::has('error'))
                            <div class="alert alert-danger text-center">
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success text-center">
                                {{Session::get('success')}}
                            </div>
                        @endif
                    </div>
                </div>
                </div><!--end row-->
                <div class="row">
                       <div class="col-md-6">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('about') ? 'has-error':''}}">
                        <label>What is the Complaint About? *</label>
                        <input type="text" name="about" class="form-control" placeholder="eg. No water problem">
                          <input type="hidden" name="tenant_id" value="{{$data->id}}">
                        @if($errors->has('about'))
              <span class="help-block">{{$errors->first('about')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <div class="col-sm-6">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('description') ? 'has-error':''}}">
                        <label>Describe the Complain *</label>
                        <textarea name="description" class="form-control" >
                        </textarea>
                        @if($errors->has('description'))
              <span class="help-block">{{$errors->first('description')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                
                 
                </div><!--end row--> 


                <div class="row">                
                <div class="col-md-6 col-md-offset-5">

                    <div class="form-group">

                        <button type="submit" class="btn btn-warning"><i class="fa fa-fw fa-send"></i>Post Complain</button>
                    </div>
                </div>

                <!-- /.form-group -->
                     </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
            </form>
          @stop