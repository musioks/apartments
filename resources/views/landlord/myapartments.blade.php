@extends('landlord.layout')
@section('pagename') My Apartments @stop
@section('content')

           <div class="table-responsive">
            <table class="table table-striped display" id="datatables">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Apartment name</th>
                  <th>Location</th>
                  <th>Name of Landlord</th>
                </tr>
              </thead>
              <tbody>
              @php 
            $i=0;
              @endphp
              @foreach($apartments as $apartments)
              @php
              $i +=1;
              @endphp
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$apartments->name}}</td>
                  <td>{{$apartments->location}}</td>
                  <td>{{$apartments->fname}}</td>
                              
                </tr>
               @endforeach
              </tbody>
            </table>
          </div>
          @stop