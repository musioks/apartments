<?php

namespace App\Http\Controllers;

use App\Announcement;
use Illuminate\Http\Request;

class AnnouncementController extends Controller
{
    public function __construct(){
        $this->middleware('admin');
    }
    public function announcements()
    {
        $notices=Announcement::all();

    return view('admin.announcements',['notices'=>$notices]);
    }

    public function createannouncement()
    {
      return view('admin.createannouncement');
    }

    public function storeAnnouncement(Request $request)
    {
        $this->validate($request,[
      'title'=>'required',
      'message'=>'required',
    ]);
        $notice=new Announcement();
        $notice->title=$request->title;
        $notice->message=$request->message;
        $notice->save();
        return redirect()->back()->with('success','Announcement saved successfully!');
    }
    public function show(Announcement $announcement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        //
    }
}
