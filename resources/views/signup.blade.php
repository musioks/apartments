<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>ONLINE APARTMENT MANAGEMENT SYSTEM</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{asset('theme/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('theme/css/font-awesome.min.css')}}">
       <script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>
</head>
<body>
<div class="container">
<div class="jumbotron" style="height:100px; padding-top:10px;">
<h2 class="text-primary text-center"><strong>ONLINE APARTMENT MANAGEMENT SYSTEM</strong></h2>
</div>
<div class="row">
<div class="col-sm-8 col-sm-offset-2">
    <div class="login-logo">
        <a href="{{url('/signup')}}"><b>Adminstrator Sign Up</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Fill in the form below to signup</p>

        <form action="{{url('/signup')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{Session::get('error')}}
                    </div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{Session::get('success')}}
                        </div>
                    @endif
                   
            </div>
            <div class="form-group has-feedback {{$errors->has('name') ? 'has-error':''}}">
                <input type="text" class="form-control" name="name" placeholder="Full Name" autofocus="">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                @if($errors->has('name'))
<span class="help-block">{{$errors->first('name')}}</span>
@endif
            </div>
             <div class="form-group has-feedback {{$errors->has('phone') ? 'has-error':''}}">
                <input type="text" class="form-control" name="phone" placeholder="Telephone No." autofocus="">
                <span class="glyphicon glyphicon-phone-alt form-control-feedback"></span>
                @if($errors->has('phone'))
<span class="help-block">{{$errors->first('phone')}}</span>
@endif
            </div>
            <div class="form-group has-feedback {{$errors->has('email') ? 'has-error':''}}">
                <input type="email" class="form-control" name="email" placeholder="Email address" autofocus="">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if($errors->has('email'))
<span class="help-block">{{$errors->first('email')}}</span>
@endif
            </div>
            <div class="form-group has-feedback {{$errors->has('password') ? 'has-error':''}}">
                <input type="password" class="form-control" name="password" placeholder="Password" autofocus="">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if($errors->has('password'))
<span class="help-block">{{$errors->first('password')}}</span>
@endif
            </div>
                <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation" autofocus="">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

        <div class="social-auth-links text-center">
        <hr>
<button type="submit"  class="btn btn-block  btn-primary"><i class="fa fa-edit"></i> Sign up </button>

        </div>
        <hr>
        <a href="{{url('/')}}" class="btn btn-default btn-block">Go to Main Page</a>
                </form>

        <!-- /.social-auth-links -->
        <hr>


    </div>
    <!-- /.login-box-body -->
    </div>
    </div>
    </div><!--end container-->


<!-- jQuery 2.2.3 -->
<script src="{{asset('theme/js/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('theme/js/bootstrap.min.js')}}"></script>


</body>
</html>