## Tenants to the system Admin: ##
1.  	Request a change of apartment
1. 	Request a maintenance petition.
1. 	Complaints.
1.	Post rent payment details
##System Admin to the tenants: ##
1.     Add a new tenant and manage his/her information.
1. 	Warn and report any tenant about his/her rental payment.
1. 	Report any interesting information (new services, taxes, etc) 
1. 	Manage the tenant maintenance request, and reporting about it. 

## Landlord to system Admin ##
*       Report about the tenant rent payments.
* 	Report about the maintenance services.
* 	Request available services.
* 	Send tenant documents.
* 	Register tenants