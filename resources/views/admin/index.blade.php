@extends('admin.layout')
@section('pagename') Dashboard @stop
@section('content')
<div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">

<a href="{{route('landlords')}}" class="btn btn-primary btn-lg">
  Landlords <span class="badge">{{count($landy)}}</span>
</a>
            </div>
              <div class="col-xs-6 col-sm-3 placeholder">

<a href="{{route('apartments')}}" class="btn btn-warning btn-lg">
  Apartments <span class="badge">{{count($apartments)}}</span>
</a>
            </div>
              <div class="col-xs-6 col-sm-3 placeholder">

<a href="{{route('houses')}}" class="btn btn-info btn-lg" >
  Houses <span class="badge">{{count($houses)}}</span>
</a>
            </div>
              <div class="col-xs-6 col-sm-3 placeholder">

<a href="{{route('tenants')}}" class="btn btn-success btn-lg" >
  Tenants <span class="badge">{{count($tenants)}}</span>
</a>
            </div>
         
          </div>

          
          @stop