@extends('admin.layout')
@section('pagename') Add Room @stop
@section('content')
          <form  enctype="multipart/form-data" method="post" action="{{url('/addhouse')}}">
                {{csrf_field()}}
                <div class="box-body">
                 <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        @if(Session::has('error'))
                            <div class="alert alert-danger text-center">
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success text-center">
                                {{Session::get('success')}}
                            </div>
                        @endif
                    </div>
                </div>
                </div><!--end row-->
                <div class="row">
                       <div class="col-md-4">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('houseID') ? 'has-error':''}}">
                        <label>Room No. *</label>
                        <input type="text" name="houseID" class="form-control" placeholder="eg. B17">
                        @if($errors->has('houseID'))
              <span class="help-block">{{$errors->first('houseID')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <div class="col-sm-4">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('size') ? 'has-error':''}}">
                        <label>Room Type *</label>
                        <select name="size" class="form-control">
                        <option value="">------Select Room type------</option>
                        <option value="Single">Single</option>
                        <option value="Bedsitter">Bedsitter</option>
                        <option value="One Bedroom">one Bedroom</option>
                        <option value="Two Bedroom">Two Bedroom</option>
                        </select>
                        @if($errors->has('size'))
              <span class="help-block">{{$errors->first('size')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                    <div class="col-sm-4">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('status') ? 'has-error':''}}">
                        <label>Room Status *</label>
                        <select name="status" class="form-control">
                        <option value="">------Select Room Status------</option>
                        <option value="Vacant">Vacant</option>
                        <option value="Occupied">Occupied</option>
                        <option value="Booked">Booked</option>
                        </select>
                        @if($errors->has('status'))
              <span class="help-block">{{$errors->first('status')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                
                 
                </div><!--end row--> 

                <div class="row">
                    <div class="col-md-6">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('rent_amount') ? 'has-error':''}}">
                        <label>Rent Per Month *</label>
                        <input type="number" name="rent_amount" class="form-control" placeholder="eg. 8500" min="0">
                        @if($errors->has('rent_amount'))
              <span class="help-block">{{$errors->first('rent_amount')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                  <div class="col-sm-6">
                 <!-- /.form-group -->
                    <div class="form-group">
                        <label>Apartment*</label>
                        <select name="apartment_id" class="form-control" required>
                        <option value="">-----Select Apartment------</option>
                         @foreach($apartments as $apartments)
                         <option value="{{$apartments->id}}">{{$apartments->name}}</option>
                         @endforeach
                        </select>
                       
                    </div>
                <!-- /.form-group -->
                </div>
                     
                </div><!--end row-->  

                <div class="row">                
                <div class="col-md-6 col-md-offset-5">

                    <div class="form-group">

                        <button type="submit" class="btn btn-success" >Add House</button>
                    </div>
                </div>

                <!-- /.form-group -->
                     </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
            </form>
          @stop