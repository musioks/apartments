<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Apartments Report</title>
@include('admin.bts')
</head>
<body>
<div class="container-fluid">
<h3 class="text-center text-success">ONLINE APARTMENT MANAGEMENT SYSTEM </h3>
<hr>
<h4 class="text-center">ALL APARTMENTS</h4>
<hr>
<table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Apartment name</th>
                  <th>Location</th>
                  <th>Name of Landlord</th>
                </tr>
              </thead>
              <tbody>
              @php 
            $i=0;
              @endphp
              @foreach($apartments as $apartments)
              @php
              $i +=1;
              @endphp
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$apartments->name}}</td>
                  <td>{{$apartments->location}}</td>
                  <td>{{$apartments->fname}}</td>
              
                </tr>
               @endforeach
              </tbody>
            </table>
                    </div>
</body>
</html>